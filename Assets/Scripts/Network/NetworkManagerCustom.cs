﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Networking.Match;
using UnityEngine.Networking.Types;

public class NetworkManagerCustom : NetworkManager {

    private int port = 7777;
    public Text textConnectionInfo;
    private Scene currentScene;
    public GameObject[] UIGroups;
    private MatchInfo hostInfo;
    public Text matchRoomNameText;
    public Transform contentRoomList;
    public GameObject roomButtonPrefab;

    #region Unity Methods

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnMySceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnMySceneLoaded;
    }

    public override void OnClientDisconnect(NetworkConnection conn)
    {
        base.OnClientDisconnect(conn);
        if (textConnectionInfo.text != null)
        {
            textConnectionInfo.text = "Disconnected or timed out.";
            ActivatePlanel("MainMenuUI");
        }
    }
    #endregion

    #region My Methods

    void OnMySceneLoaded(Scene scene, LoadSceneMode mode)
    {
        SetInitialReferences();
    }

    void SetInitialReferences()
    {
        currentScene = SceneManager.GetActiveScene();

        if(currentScene.name == "MainMenu")
        {
            ActivatePlanel("MainMenuUI");
        }
        else
        {
            ActivatePlanel("ClearPanel");
            OnClickClearConnectionTextInfo();
        }
    }

    public void ActivatePlanel(string panelName)
    {
        foreach (GameObject panelGo in UIGroups)
        {
            if (panelGo.name.Equals(panelName))
            {
                panelGo.SetActive(true);
            }
            else
            {
                panelGo.SetActive(false);
            }
        }
    }

    void SetPort()
    {
        NetworkManager.singleton.networkPort = port;
    }

    public void OnClickClearConnectionTextInfo()
    {
        textConnectionInfo.text = string.Empty;
    }

    public void OnClickDisconnectFromNetwork()
    {
        NetworkManager.singleton.StopHost();
        NetworkManager.singleton.StopServer();
        NetworkManager.singleton.StopClient();
    }

    public void OnClickExitGame()
    {
        Application.Quit();
    }

    public void DisableMatchMaker()
    {
        NetworkManager.singleton.StopMatchMaker();
    }

    public void EnableMatchMaker()
    {
        DisableMatchMaker(); //first disable matchmaker in-case already running
        SetPort();
        NetworkManager.singleton.StartMatchMaker();
    }

    public void OnClickCreateMatch()
    {
        NetworkManager.singleton.matchMaker.CreateMatch(matchRoomNameText.text, 20, true, "", "", "", 0, 0, OnInternetCreateMatch);
    }

    void OnInternetCreateMatch(bool success, string extendedInfo, MatchInfo matchInfo)
    {
        if(success)
        {
            textConnectionInfo.text = "Create Match Succeeded.";
            hostInfo = matchInfo;
            NetworkServer.Listen(hostInfo, NetworkManager.singleton.matchPort);
            NetworkManager.singleton.StartHost(hostInfo);
        }
        else
        {
            textConnectionInfo.text = "Create Match Failed.";
        }
    }

    void ClearContentRoomList()
    {
        foreach(Transform child in contentRoomList)
        {
            Destroy(child.gameObject);
        }
    }

    public void OnClickFindInternetMatch()
    {
        ClearContentRoomList();
        NetworkManager.singleton.matchMaker.ListMatches(0, 10, "", true, 0, 0, OnInternetMatchList);
    }

    void OnInternetMatchList(bool success, string extendedInfo, List<MatchInfoSnapshot> matches)
    {
        if (success)
        {
            if(matches.Count != 0)
            {
                foreach (MatchInfoSnapshot matchesAvailable in matches) //Creates UI button for each available match
                {
                    GameObject roomButton = Instantiate(roomButtonPrefab) as GameObject;
                    roomButton.GetComponentInChildren<Text>().text = matchesAvailable.name;
                    roomButton.GetComponent<Button>().onClick.AddListener(delegate
                    { JoinInternetMatch(matchesAvailable.networkId, "", "", "", 0, 0, OnJoinInternetMatch); });
                    roomButton.GetComponent<Button>().onClick.AddListener(delegate
                    { ActivatePlanel("PanelAttemptingToConnect"); });
                    roomButton.transform.SetParent(contentRoomList, false);

                }
            }

            else
            {
                textConnectionInfo.text = "No matches available.";
            }
        }

        else
        {
            textConnectionInfo.text = "Could not connect to match maker.";
        }
    }

    public void JoinInternetMatch(NetworkID netID, string password, string pubClientAddress, string privClientAddress, int eloScore, int reqDomain, NetworkMatch.DataResponseDelegate<MatchInfo> callback)
    {
        NetworkManager.singleton.matchMaker.JoinMatch(netID, password, pubClientAddress, privClientAddress, eloScore, reqDomain, OnJoinInternetMatch);
    }

    void OnJoinInternetMatch(bool success, string extendedInfo, MatchInfo matchInfo)
    {
        if(success)
        {
            hostInfo = matchInfo;
            NetworkManager.singleton.StartClient(hostInfo);
        }
        else
        {
            textConnectionInfo.text = "Join Match Failed";
        }
    }
    #endregion
}
