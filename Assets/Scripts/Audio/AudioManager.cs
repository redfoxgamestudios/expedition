﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{

    public Audio[] sounds;
    public Audio[] bgMusic;

    int song;


    void Awake ()
    {
		foreach (Audio s in sounds)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;
            s.source.pitch = s.pitch;
        }
        foreach (Audio m in bgMusic)
        {
            m.source = gameObject.AddComponent<AudioSource>();
            m.source.clip = m.clip;

            m.source.volume = m.volume;
            m.source.pitch = m.pitch;
            m.source.loop = m.loop;
        }
        PlayBGMusic("Song01");
	}
	
	public void Play (string name)
    {
        Audio s = Array.Find(sounds, sound => sound.name == name);
        s.source.Play();
    }

    public void PlayBGMusic(string name)
    {
        Audio m = Array.Find(bgMusic, sound => sound.name == name);
        m.source.Play();
    }
}
