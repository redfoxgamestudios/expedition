﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class PlayerInputManager : NetworkBehaviour {

    public GameObject InGameMenu;

	void Start ()
    {
		
	}
	

	void Update ()
    {
		if(!isLocalPlayer)
        {
            return;
        }

        if(Input.GetButtonDown("Cancel"))
        {
            InGameMenu.SetActive(!InGameMenu.activeInHierarchy);
        }
	}
}
