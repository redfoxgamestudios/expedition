﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityStandardAssets.Characters.FirstPerson;
using UnityEngine.UI;


public class PlayerHealth : NetworkBehaviour {

    [SyncVar(hook = "UpdateHealthBar")]

    public int playerHealth = 100;
    public GameObject[] characterModel;
    public GameObject respawnButton;
    public GameObject deathScreen;
    public GameObject healthBarGo;
    public RectTransform healthBarRect;
    public Text healthText;
     
	void Start ()
    {
        UpdateHealthBar(playerHealth);
    }

    void UpdateHealthBar(int pHealth)
    {
        healthText.text = pHealth.ToString();
    }

    public void DeductHealth(int damage)
    {
        if(!isServer)
        {
            return;
        }

        playerHealth -= damage;

        if(playerHealth <= 0) //check if player is dead
        {
            playerHealth = 0; //player cant have negative health
            RpcDeactivatePlayer();
        }
    }

    [ClientRpc]
    void RpcDeactivatePlayer()
    {
        GetComponent<FirstPersonController>().enabled = false;
        GetComponent<NetworkTransform>().enabled = false;
        GetComponent<PlayerShoot>().enabled = false;
        GetComponent<CharacterController>().enabled = false;
        //healthBarGo.SetActive(false);

        foreach (GameObject go in characterModel)
        {
            go.SetActive(false);
        }

        if(isLocalPlayer)
        {
            deathScreen.SetActive(true);
            Cursor.visible = true; //show mouse cursor
            Cursor.lockState = CursorLockMode.Confined;
        }
    }

    public void Respawn()
    {
        Cursor.visible = false; //hide mouse cursor
        Cursor.lockState = CursorLockMode.Locked;
        playerHealth = 100;
        GetComponent<NetworkTransform>().enabled = true;
        GetComponent<PlayerShoot>().enabled = true;
        GetComponent<CharacterController>().enabled = true;

        if(isLocalPlayer)
        {
            GetComponent<FirstPersonController>().enabled = true;
            deathScreen.SetActive(false);
            SelectSpawnPoint();
        }

        else
        {
            StartCoroutine(MakePlayerModelVisible());
        }
    }

    void SelectSpawnPoint()
    {
        Transform chosenSpawnPoint = NetworkManager.singleton.startPositions[Random.Range(0, NetworkManager.singleton.startPositions.Count)];
        transform.position = chosenSpawnPoint.position;
        transform.rotation = chosenSpawnPoint.rotation;
    }

    IEnumerator MakePlayerModelVisible()
    {
        yield return new WaitForSeconds(1.5f);

        foreach (GameObject go in characterModel)
        {
            go.SetActive(true);
        }
    }

}
