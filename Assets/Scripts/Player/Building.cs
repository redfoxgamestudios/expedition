﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Building : MonoBehaviour
{
    public Vector3[] snappoints;
    public GameObject buildingPrefab;

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.B))
        {
            BuildingManager.instance.BuildingPrefab = buildingPrefab;
            BuildingManager.instance.buildingMode = true;
        }
    }

}