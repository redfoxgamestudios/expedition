﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class BuildingManager : NetworkBehaviour
{

    private GameObject buildingPrefab;

    public Camera playerCam;

    public Material TransparentGreen;

    public Material TransparentRed;

    public GameObject Preview;

    public bool buildingMode = false;

    public float minGridSizeX;
    public float minGridSizeZ;

    public static BuildingManager instance;

    public GameObject BuildingPrefab
    {
        set
        {
            buildingPrefab = value;
            //Destroy(Preview);
        }
    }

    bool valid = false;

	void Start ()
    {
        instance = this;
	}
	

	void FixedUpdate ()
    {
        if (!buildingMode)
        {
            if (Preview != null)
            {
                Destroy(Preview);
            }
            return;
        }

        Ray ray = playerCam.ScreenPointToRay(Input.mousePosition);

        RaycastHit hit;

        if (Physics.Raycast(ray, out hit))
        {
            if (Preview == null)
            {
                CmdSpawnPreview(hit.point, Quaternion.Euler(0f, 0f, 0f));
                Preview = Instantiate(buildingPrefab, hit.point, Quaternion.Euler(0f, 0f, 0f));
                MeshRenderer[] renderers = Preview.GetComponentsInChildren<MeshRenderer>();

                foreach (MeshRenderer renderer in renderers)
                {
                    renderer.material = TransparentRed;
                }

                MeshRenderer meshRenderer = Preview.GetComponentInChildren<MeshRenderer>();

                Preview.GetComponent<Collider>().enabled = false;

                //DISABLE UNNESSESSARY SCIRPTS PER EXAMPLE BELOW
                //Facility facility = Preview.GetComponent<Facility>();

                //if (facility != null)
                //{
                //    facility.enabled = false;
                //}

            }
            else
            {

                Collider[] colliders = Physics.OverlapSphere(hit.point, 15f);

                Vector3 closest = hit.point;

                float currentDistance = float.MaxValue;

                foreach (Collider collider in colliders)
                {

                    Building building = collider.transform.GetComponent<Building>();

                    if (building != null)
                    {

                        foreach (Vector3 snappoint in building.snappoints)
                        {
                            Vector3 point = building.transform.position + snappoint;

                            float distance = Vector3.Distance(hit.point, point);

                            if (distance < currentDistance)
                            {
                                closest = point;

                                currentDistance = distance;
                            }
                        }
                    }
                }

                closest.y = 0f;

                closest.z = Mathf.FloorToInt(closest.z / minGridSizeZ) * minGridSizeZ;

                closest.x = Mathf.RoundToInt(closest.x / minGridSizeX) * minGridSizeX;

                Preview.transform.position = closest;

                Vector3 pointToCheck = Preview.transform.position;

                valid = false;

                if (Physics.Raycast(new Ray(pointToCheck, Vector3.up), 5f))
                {
                    valid = true;
                }

                if (Physics.Raycast(new Ray(pointToCheck, Vector3.down), 5f))
                {
                    valid = true;
                }

                if (Physics.Raycast(new Ray(pointToCheck, Vector3.right), 13f))
                {
                    valid = true;
                }

                if (Physics.Raycast(new Ray(pointToCheck, Vector3.left), 13f))
                {
                    valid = true;
                }

                //colliders = Physics.OverlapBox(Preview.transform.position + (world.rotation * bounds.center), bounds.extents);

                //foreach (Collider collider in colliders)
                //{
                //    Facility facility = collider.transform.GetComponent<Facility>();

                //    if (facility != null)
                //    {
                //        valid = false;
                //        break;
                //    }
                //}

                if (valid)
                {
                    Preview.SetActive(true);

                    MeshRenderer[] renderers = Preview.GetComponentsInChildren<MeshRenderer>(true);

                    foreach (MeshRenderer renderer in renderers)
                    {
                        renderer.material = TransparentGreen;
                    }
                }
                else
                {
                    MeshRenderer[] renderers = Preview.GetComponentsInChildren<MeshRenderer>(true);

                    foreach (MeshRenderer renderer in renderers)
                    {
                        renderer.material = TransparentRed;
                    }
                    //Preview.SetActive(false); //Sets Preview as inactive if not valid placement
                }
            }
        }
    }

    [Command]//Command ONLY run on server NOT host
    void CmdSpawnPreview(Vector3 pos, Quaternion rot)
    {
        GameObject Preview = Instantiate(buildingPrefab, pos, rot);
        NetworkServer.Spawn(Preview);
    }

    void Update()
    {
        if (isLocalPlayer && Input.GetMouseButtonDown(1))
        {
            if (valid)
            {
                CmdSpawnbuildingPrefab(Preview.transform.position, Preview.transform.rotation);
            }
        }
    }

    [Command]//Command ONLY run on server NOT host
    void CmdSpawnbuildingPrefab(Vector3 pos, Quaternion rot)
    {
        Instantiate(buildingPrefab, pos, rot);
    }
}
