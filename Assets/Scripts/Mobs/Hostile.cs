using UnityEngine;
using System.Collections;

public class Hostile : Mob
{
	// Use this for initialization
	void Start()
	{


	}

	// Update is called once per frame
	void Update()
	{
        Transform player = FindPlayer();

        if (Vector3.Distance(player.position, transform.position) < detectionDistance)
            MoveTo(player);

	}
}
