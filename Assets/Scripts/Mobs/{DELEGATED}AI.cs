﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AI : MonoBehaviour {

    public bool Hostile = true;

	// Use this for initialization
	void Start () {

        InvokeRepeating("Repeat", 0.0f, 5f);

	}
	
	// Update is called once per frame
	void Update () {



	}

    private void FixedUpdate()
    {

        if (Hostile) {
            
        }

    }

    public void MoveTo(Vector3 p)
    {

        NavMeshAgent agent = GetComponent<NavMeshAgent>();
        agent.destination = p;

    }

    private void Repeat() 
    {
        float x = Random.value * 10.0f - 10.0f;
        float y = Random.value * 10.0f - 10.0f;
        float z = Random.value * 10.0f - 10.0f;

        MoveTo(new Vector3(x, y, z));
    }
}
