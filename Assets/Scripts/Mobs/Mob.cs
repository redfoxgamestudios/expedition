using UnityEngine;
using System.Collections;
using UnityEngine.AI;

public class Mob : MonoBehaviour
{

    public float health = 100.0f;
    public float damage = 100.0f;
    public float speed = 10.0f;
    public float jumpHeight = 1.0f;
    public enum active {Day, Night, DayNight};
    public float detectionDistance = 10.0f;

	// Use this for initialization
	void Start()
	{
        
	}

	// Update is called once per frame
	void Update()
	{

	}

    /* 
     * Function that moves agent.
     * Input: Transform object.
     * Result: Agent moves towards given postion.
     */
    public void MoveTo(Transform t)
    {

        NavMeshAgent agent = GetComponent<NavMeshAgent>();
        agent.destination = t.position;

    }

    /* 
     * Function that moves agent. (OVERLOAD)
     * Input: Position object.
     * Result: Agent moves towards given postion.
     */
    public void MoveTo(Vector3 p)
    {

        NavMeshAgent agent = GetComponent<NavMeshAgent>();
        agent.destination = p;

    }

    /*
     * Function that finds the nearest player's position.
     * Output: Player's transform.
     */
    public Transform FindPlayer() {

        return GameObject.FindGameObjectWithTag("Player").transform;

    }


}
